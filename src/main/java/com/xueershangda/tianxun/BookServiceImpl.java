package com.xueershangda.tianxun;

import com.vteba.tutorial.Book;
import com.vteba.tutorial.BookService;
import com.vteba.tutorial.GetBookRequest;
import com.vteba.tutorial.QueryBooksRequest;
import org.apache.dubbo.common.stream.StreamObserver;

import java.util.concurrent.CompletableFuture;

public class BookServiceImpl implements BookService {
    // 这个是非stream接口的
    @Override
    public Book getBook(GetBookRequest request) {
        return Book.newBuilder()
                .setAuthor("yinlei")
                .setIsbn(123465L)
                .setTitle("好书啊").build();
    }

    @Override
    public CompletableFuture<Book> getBookAsync(GetBookRequest request) {
        return CompletableFuture.completedFuture(getBook(request));
    }

    // 这个是Dubbo3Generator生成的stream的代码，0.0.3 版本的Dubbo3Generator，区分了stream接口和非stream接口。这个是stream接口
    @Override
    public void queryBooks(QueryBooksRequest request, StreamObserver<Book> responseObserver) {
        String authorPrefix = request.getAuthorPrefix();
        if (authorPrefix != null) {
            Book book = Book.newBuilder().setAuthor(authorPrefix).build();
            responseObserver.onNext(book);
        }
    }

    // 下面是Dubbo3Generator 0.0.2版本产生的stream接口代码，0.0.3版本的接口代码发生了变化，见上面
//    @Override
//    public Book queryBooks(QueryBooksRequest request) {
//        String authorPrefix = request.getAuthorPrefix();
//        if (authorPrefix != null) {
//            return Book.newBuilder().setAuthor(authorPrefix).build();
//        }
//        return null;
//    }
//
//    @Override
//    public CompletableFuture<Book> queryBooksAsync(QueryBooksRequest request) {
//        return CompletableFuture.completedFuture(queryBooks(request));
//    }
}
