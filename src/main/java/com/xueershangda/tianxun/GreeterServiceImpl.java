package com.xueershangda.tianxun;

import io.grpc.examples.helloworld.Greeter;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;

public class GreeterServiceImpl implements Greeter {

    // 0.0.3 版本的Dubbo3Generator，区分了stream接口和非stream接口。这个是非stream接口
    @Override
    public HelloReply sayHello(HelloRequest request) {
        String name = request.getName();
        return HelloReply.newBuilder().setMessage(name).build();
    }
}
